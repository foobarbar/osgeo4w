# Create Windows VM

In order to create a VirtualBox machine ready to build OSGEO4W package you need to follow this steps

### Create Windows VirtualBox machine

We use packer to do so.

```shell
$ git submodule update --init --recursive
$ cd vm_windows/packer-windows
$ packer build -only=virtualbox-iso -var disk_size=71680 windows_10.json
```

It takes several minutes to build the VirtualBox machine. Once finished, the file `windows_10_virtualbox.box` should exists in `vm_windows/packer-windows` folder.

### Load and provision the VirtualBox machine

You need to install `winrm` before loading the virtual box machine

```shell
$ vagrant plugin install winrm
$ vagrant plugin install winrm-elevated
```

Then import previously generated virtual box machine and run it

```shell
$ vagrant box add win10-vs2017 ./windows_10_virtualbox.box
$ cd ..
$ vagrant up
```

It loads the virtual machine and installs everything needed to build OSGEO4W package

The process is quite long. If something bad happen or you want to restart the provisionning for some reason, you can type the following
```shell
$ vagrant up --provision
```

### First build

VirtualBox machine login informations are:

- login: vagrant
- password: vagrant

Once started, just click on `C:/cygwin64/Cygwin.bat`. It launches the cygwin terminal. From here you can build any packages, QGIS for instance

```shell
$ git clone https://gitlab.com/Oslandia/osgeo4w.git
$ cd osgeo4w/packages/qgis
$ cmd /c package.cmd local
```

Regarding QGIS, you can execute the file `C:/OSGeo4W/bin/qgis-oslandia.bat.tmpl` to start QGIS. It sets correctly the environment before running QGIS.

Folder `vm_windows` is currently shared with the Windows Guest in `C:\vagrant.

