::--------- Package settings --------
:: package name
set P=cgal
:: version
set V=4.14.3
:: package version
set B=1

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1

set HERE=%CD%

rd /s /q c:\install
mkdir c:\install

set BUILD_DEPS=boost-devel-vc14 gmp mpfr

::dir c:\osgeo4w64\include
::dir c:\osgeo4w64\lib

wget --progress=bar:force https://github.com/CGAL/cgal/archive/releases/CGAL-%V%.tar.gz  || goto :err
tar xzf CGAL-%V%.tar.gz  || goto :error
cd cgal-releases-CGAL-%V%  || goto :error
rd /s /q build
mkdir build
cd build
cmake -G "NMake Makefiles" ^
    -DCMAKE_BUILD_TYPE=RelWithDebInfo ^
    -DCMAKE_INSTALL_PREFIX=C:/install ^
    -DGMP_INCLUDE_DIR=c:/osgeo4w64/include ^
    -DGMP_LIBRARIES=c:/osgeo4w64/lib/libgmp-10.lib ^
    -DMPFR_INCLUDE_DIR=c:/osgeo4w64/include ^
    -DMPFR_LIBRARIES=c:/osgeo4w64/lib/libmpfr-4.lib ^
    -DBOOST_INCLUDEDIR=c:/osgeo4w64/include/boost-1_63 ^
    -DBOOST_LIBRARYDIR=c:/osgeo4w64/lib ^
    -DWITH_CGAL_ImageIO=OFF ^
    .. || goto :error
::    -DCMAKE_VERBOSE_MAKEFILE=ON ^
::    -DZLIB_ROOT=c:\osgeo4w64 ^
	  
cmake --build . --target install VERBOSE=1 || goto :error

move c:\inslall\lib\*.dll c:\inslall\bin

:: binary archive
c:\cygwin64\bin\tar.exe  -C c:\install -cjvf %PKG_BIN% lib share include bin || goto :error

:: source archive
c:\cygwin64\bin\tar.exe  -C %HERE% --transform 's,^,osgeo4w/,' -cvjf %PKG_SRC% package.cmd setup.hint || goto :error

::--------- Installation
scp %PKG_BIN% %PKG_SRC% %R% || goto :error
cd %HERE%
scp setup.hint %R% || goto :error
goto :EOF

:error
echo Build failed
exit /b 1
  
  
