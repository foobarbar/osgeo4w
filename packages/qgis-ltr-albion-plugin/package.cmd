::--------- Package settings --------
:: package name
set P=qgis-ltr-albion-plugin
:: version
set V=4.2.3
:: package version
set B=8

set HERE=%CD%

::--------- Prepare the environment
set BUILD_DEPS=python3-core python3-pip

call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%
set PYTHONPATH=%PYTHONPATH%;%HERE%

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat || goto :error

echo ################################
python --version
echo ################################
python -m pip install gitpython || goto :error

wget https://github.com/Oslandia/albion/releases/download/r2.3beta/Albion.zip -O albion.zip || goto :error
unzip albion.zip || goto :error
mkdir install || goto :error
sed -i '' -e "/build_doc/d" Albion/package.py
python -m Albion.package -i install -n || goto :error

c:\cygwin64\bin\tar.exe --transform 's,install/,apps/qgis-ltr/python/plugins/,' -cvjf %PKG_BIN% install || goto :error


::--------- Installation
scp %PKG_BIN% %R% || goto :error
scp setup.hint %R% || goto :error
goto :EOF


:error
echo Build failed
exit /b 1
