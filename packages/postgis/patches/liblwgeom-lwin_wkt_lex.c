diff --git liblwgeom/lwin_wkt_lex.c liblwgeom/lwin_wkt_lex.c
index 9d918ae..20b2e2a 100644
--- liblwgeom/lwin_wkt_lex.c
+++ liblwgeom/lwin_wkt_lex.c
@@ -902,7 +902,7 @@ static void wkt_lexer_unknown()
  * down here because we want the user's section 1 to have been scanned first.
  * The user has a chance to override it with an option.
  */
-#include <unistd.h>
+//#include <unistd.h>
 #endif
 
 #ifndef YY_EXTRA_TYPE
