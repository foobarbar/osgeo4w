--- "liblwgeom\\ptarray.c.orig"	2019-02-20 13:58:41.360743200 +0100
+++ "liblwgeom\\ptarray.c"	2019-02-20 14:20:54.132190700 +0100
@@ -1500,14 +1502,14 @@ ptarray_remove_repeated_points_in_place(POINTARRAY *pa, double tolerance, uint32
 			if (last_point && n_points_out > 1 && tolerance > 0.0 && dsq <= tolsq)
 			{
 				n_points_out--;
-				p_to -= pt_size;
+				p_to = (char*)p_to - pt_size;
 			}
 		}
 
 		/* Compact all remaining values to front of array */
 		memcpy(p_to, pt, pt_size);
 		n_points_out++;
-		p_to += pt_size;
+		p_to = (char*)p_to + pt_size;
 		last = pt;
 	}
 	/* Adjust array length */
