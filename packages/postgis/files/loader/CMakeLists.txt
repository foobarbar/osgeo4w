find_package(Iconv)
find_package(Gettext)

include_directories(BEFORE ${ICONV_INCLUDE_DIR})

if (ICONV_LIBRARY)
else()
    set(ICONV_LIBRARY "")
endif()

if(MSVC)
  add_definitions(/Dstrcasecmp=_stricmp)
  add_definitions(/Dstrncasecmp=_strnicmp)
endif()

add_executable(shp2pgsql 
    $<TARGET_OBJECTS:liblwgeom>
    shp2pgsql-core.c 
    shp2pgsql-core.h
    shp2pgsql-cli.c
    shpopen.c
    dbfopen.c
    shapefil.h
    shpcommon.h
    shpcommon.c
    safileio.c
    getopt.c
    )
target_link_libraries(shp2pgsql
	${GEOS_LIBRARY}
	${PROJ4_LIBRARY}
    ${POSTGRESQL_LIBRARIES}
    ${LIBMFLAG}
    ${ICONV_LIBRARY}
    )


	message("postgresql libraries ${POSTGRESQL_LIBRARIES}")
add_executable(pgsql2shp
    $<TARGET_OBJECTS:liblwgeom>
    pgsql2shp-core.c 
    pgsql2shp-core.h 
    pgsql2shp-cli.c
	shpopen.c
    dbfopen.c
    shapefil.h
    shpcommon.h
    shpcommon.c
    safileio.c
    getopt.c
    )
target_link_libraries(pgsql2shp
	${GEOS_LIBRARY}
	${PROJ4_LIBRARY}
    ${POSTGRESQL_LIBRARIES}
	${LIBPQ_LIBRARY}
    ${LIBMFLAG}
    ${ICONV_LIBRARY}
    )
