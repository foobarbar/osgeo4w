::--------- Package settings --------
:: package name
set P=gmsh
:: version
set V=4.5.5
:: package version
set B=1

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1

wget http://gmsh.info/bin/Windows/gmsh-4.5.5-Windows64.zip
unzip gmsh-4.5.5-Windows64.zip


:: binary archive
tar --transform 's,gmsh-4.5.5-Windows64,bin,' -cvjf %PKG_BIN% gmsh-4.5.5-Windows64/gmsh.exe

::--------- Installation
scp %PKG_BIN% %R%
cd %HERE%
scp setup.hint %R%
